export class Contact {
	id: number;
	name: string;
	company: string;
	email: string;
	address: string;
	job_title: string;
	phone: string;
	comment: string;
	last_date_contacted: string;


	// public createContact(contact: Contact){}

	// public updateContact(contact: Contact){}

	// public deleteContact(id: number){}

	// public getContactById(id: number){}

	// public getContacts(url?: string){}
}
