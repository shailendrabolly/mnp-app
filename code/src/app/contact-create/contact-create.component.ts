import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from '../contact.service';
import { Router,ActivatedRoute } from '@angular/router';
@Component({
	selector: 'app-contact-create',
	templateUrl: './contact-create.component.html',
	styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {
	contactForm: FormGroup;
	submitted = false;
	constructor(private formBuilder: FormBuilder, private apiService: ContactService, private router: Router) { }

	ngOnInit() {
		this.contactForm = this.formBuilder.group({
			name: ['', Validators.required],
			address: ['', Validators.required],
			last_date_contacted: ['', Validators.required],
			job_title: ['', Validators.required],
			phone: ['', Validators.required],
			email: ['', [Validators.required,Validators.email]],
			company: ['', Validators.required],
			comment: ['', Validators.required],
			

		});

	}

	get fval() {
		return this.contactForm.controls;
	}

	submitData(){
		this.submitted = true;
		if (this.contactForm.invalid) {
			return;
		}else{
			this.apiService.createContact(this.contactForm.value).subscribe((res)=>{
				console.log("Created a contact");
				this.router.navigate(['/contacts'])
			});
			
		}
		
	}

}
