import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';

const routes: Routes = [
{ path: 'contacts', component: ContactListComponent },
{ path: 'contact-add', component: ContactCreateComponent },
// { path: 'contact-edit', component: ContactEditComponent },
{ path: '', redirectTo: 'contact/:id', pathMatch: 'full' },
{ path: 'contact/:id', component: ContactEditComponent },


];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
