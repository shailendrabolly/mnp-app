import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from './contact';


@Injectable({
	providedIn: 'root'
})
export class ContactService {
	apiURL: string = 'http://localhost/api.php';
	constructor(private httpClient: HttpClient) { 


	}

	public getContacts(url?: string){

		return this.httpClient.get<Contact[]>(`${this.apiURL}/contact`);

	}

	public getContactsId(id: number){
		return this.httpClient.get(`${this.apiURL}/contact/${id}`);
	}

	public createContact(contact: Contact){
		return this.httpClient.post(`${this.apiURL}/contact/`,contact);
	}


	public updateContact(contact: Contact){
		return this.httpClient.put(`${this.apiURL}/contact/${contact.id}`,contact);
	}
}
