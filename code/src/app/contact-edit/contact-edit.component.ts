import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
	selector: 'app-contact-edit',
	templateUrl: './contact-edit.component.html',
	styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {

	contactData:any;
	id;
	contactForm: FormGroup;
	submitted = false;
	constructor(private _Activatedroute:ActivatedRoute,private formBuilder: FormBuilder, private apiService: ContactService, private router: Router) { }

	ngOnInit(): void {
		this.id=this._Activatedroute.snapshot.paramMap.get("id");
		this.apiService.getContactsId(this.id).subscribe((res)=>{
			this.contactData = res;
			this.contactForm.patchValue({
				id: this.contactData.id,
				name: this.contactData.name,
				address: this.contactData.address,
				last_date_contacted: this.contactData.last_date_contacted,
				job_title: this.contactData.job_title,
				phone: this.contactData.phone,
				email: this.contactData.email,
				company: this.contactData.company,
				comment: this.contactData.comment,
			});
			
		});   

		


		this.contactForm = this.formBuilder.group({
			id: ['', Validators.required],
			name: ['', Validators.required],
			address: ['', Validators.required],
			last_date_contacted: ['', Validators.required],
			job_title: ['', Validators.required],
			phone: ['', Validators.required],
			email: ['', [Validators.required,Validators.email]],
			company: ['', Validators.required],
			comment: ['', Validators.required],
			

		});
	}

	get fval() {
		return this.contactForm.controls;
	}

	submitData(){
		this.submitted = true;
		if (this.contactForm.invalid) {
			return;
		}else{
			this.apiService.updateContact(this.contactForm.value).subscribe((res)=>{
				console.log("Created a contact");
				this.router.navigate(['/contacts'])
			});
			
		}
		
	}

}
