import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
@Component({
	selector: 'app-contact-list',
	templateUrl: './contact-list.component.html',
	styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
    contactData:any[] = [];
	constructor(private apiService: ContactService) { }

	ngOnInit(): void {

		this.apiService.getContacts().subscribe((res)=>{
			this.contactData = res;
		});      
	}

}
